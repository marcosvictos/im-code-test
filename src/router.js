import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Prime from './views/Prime.vue'
import BinarySearch from './views/Binary.vue'
import SWAPI from './views/SWAPI.vue'
import CharacterDetail from './views/CharacterDetail.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/prime',
      name: 'prime',
      component: Prime
    },
    {
      path: '/bs',
      name: 'binary-search',
      component: BinarySearch
    },
    {
      path: '/characters',
      name: 'characters',
      component: SWAPI
    },
    {
      path: '/character/:url',
      name: 'character',
      component: CharacterDetail,
      props: true
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
